export const viewports = {
    mobile: 400,
    medium: 1024,
};

export const queries = {
    mobile: `@media screen and (max-width: ${viewports.mobile - 1}px)`,
    medium: `@media screen and (min-width: ${viewports.medium}px)`,
    desktop: `@media screen and (min-width: ${viewports.mobile}px)`,
};
