import Head from 'next/head';

type Props = {
    title?: string;
    description?: string;
};

export const Meta = ({ title, description }: Props) => {
    const defaultTitle = 'Prague Labs';

    return (
        <Head>
            <title>{title || defaultTitle}</title>
            <meta property="og:title" content={title || defaultTitle} />
            {!!description && (
                <>
                    <meta name="description" content={description} />
                    <meta property="og:description" content={description} />
                </>
            )}
        </Head>
    );
};
