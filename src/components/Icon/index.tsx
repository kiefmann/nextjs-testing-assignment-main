import styled from 'styled-components';

const requireIcon = (name: string) => {
    return require(`./assets/Icon-${name}.svg`).default.src;
};

const icons = {
    action: requireIcon('Action'),
    bed: requireIcon('Bed'),
    seats: requireIcon('Seats'),
    shower: requireIcon('Shower'),
    toilet: requireIcon('Toilet'),
    expand: requireIcon('Expand'),
} as const;

type IconName = keyof typeof icons;

type Props = {
    name: IconName;
};

export const Icon = styled.span<Props>`
    font-size: 20px;
    width: 1em;
    height: 1em;
    background: url(${(p) => icons[p.name]}) no-repeat;
    background-position: center;
    display: inline-block;
`;
