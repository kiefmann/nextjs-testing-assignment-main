import styled from 'styled-components';
import { queries } from '../../../styles/viewports';

export const Container = styled.div`
    max-width: 1240px;
    box-sizing: content-box;
    padding: 0 16px;
    margin-left: auto;
    margin-right: auto;

    ${queries.desktop} {
        padding: 0 32px;
    }
`;
