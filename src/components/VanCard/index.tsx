import Image from 'next/image';
import styled from 'styled-components';
import { Van, VanType } from '../../types/van';
import { Icon } from '../Icon';

type Props = Van;

export const VanCard = ({
    price,
    name,
    vehicleType,
    instantBookable,
    location,
    passengersCapacity,
    pictures,
    shower,
    sleepCapacity,
    toilet,
}: Props) => {
    const vehicleTypeNames: Record<VanType, string> = {
        Alcove: 'Přívěs',
        BuiltIn: 'Vestavba',
        Campervan: 'Campervan',
        Intergrated: 'Integrál',
    };

    return (
        <Wrapper>
            <ImageWrapper>
                {!!pictures?.length && (
                    <Image
                        src={pictures[0]}
                        alt="Vozidlo"
                        sizes="(max-width: 700px) 100vw, (max-width: 1024px) 50vw, 33vw"
                        layout="fill"
                        objectFit="cover"
                    />
                )}
            </ImageWrapper>
            <Content>
                <Type>{vehicleTypeNames[vehicleType]}</Type>
                <Name>{name}</Name>
                <Separator></Separator>
                <Location>{location}</Location>
                <Properties>
                    <Property>
                        <Icon name="seats" />
                        {passengersCapacity}
                    </Property>
                    <Property>
                        <Icon name="bed" />
                        {sleepCapacity}
                    </Property>
                    {toilet && (
                        <Property>
                            <Icon name="toilet" />
                        </Property>
                    )}
                    {shower && (
                        <Property>
                            <Icon name="shower" />
                        </Property>
                    )}
                </Properties>
                <Separator></Separator>
                <PriceWrapper>
                    <PriceLabel>Cena od</PriceLabel>
                    <Price>
                        {price} Kč/den
                        {instantBookable && <ActionIcon name="action" />}
                    </Price>
                </PriceWrapper>
            </Content>
        </Wrapper>
    );
};

const Wrapper = styled.article`
    display: flex;
    flex-direction: column;
    border: 1px solid var(--beige);
    border-radius: 8px;
    overflow: hidden;
`;

const ImageWrapper = styled.div`
    height: 190px;
    width: 100%;
    position: relative;
`;

const Content = styled.div`
    padding: 12px 16px 17px;
    flex: 1 0 auto;
`;

const Type = styled.p`
    color: var(--orange);
    text-align: left;
    font: normal normal bold 12px/12px Roboto;
    letter-spacing: 1px;
    text-transform: uppercase;
    margin-bottom: 2px;
`;

const Name = styled.h2`
    color: var(--dark-blue);
    font: normal normal bold 24px/24px Roboto;
    letter-spacing: 0px;
    margin-bottom: 5px;
`;

const Separator = styled.div`
    border-bottom: 1px solid var(--beige);
`;

const Location = styled.p`
    color: var(--dark-blue);
    font: normal normal normal 14px/16px Roboto;
    letter-spacing: 0px;
    margin: 9px 0;
`;

const Properties = styled.div`
    display: flex;
    gap: 13px;
    margin-bottom: 14px;
`;

const Property = styled.div`
    display: flex;
    align-items: center;
    gap: 4px;
    color: var(--dark-blue);
    text-align: left;
    font: normal normal normal 14px/16px Roboto;
    letter-spacing: 0px;
`;

const PriceWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-top: 17px;
`;

const PriceLabel = styled.span`
    color: var(--dark-grey);
    font: normal normal normal 16px/16px Roboto;
    letter-spacing: 0px;
`;

const Price = styled.span`
    color: var(--dark-blue);
    text-align: right;
    font: normal normal bold 16px/16px Roboto;
    letter-spacing: 0px;
    display: flex;
    align-items: center;
    gap: 8px;
`;

const ActionIcon = styled(Icon)`
    font-size: 16px;
`;
