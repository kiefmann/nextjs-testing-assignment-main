import {
    createContext,
    ReactNode,
    useCallback,
    useContext,
    useMemo,
    useState,
} from 'react';
import styled, { css } from 'styled-components';
import { queries } from '../../../styles/viewports';
import { VanType } from '../../types/van';
import { Container } from '../Container';
import { Icon } from '../Icon';
import { Section } from '../Section';
import {
    Slider,
    TextField,
    InputAdornment,
    Select,
    MenuItem,
} from '@mui/material';
import { clamp } from '../../utils/clamp';

type Filters = {
    price: {
        min: number;
        max: number;
    };
    types: VanType[];
    instantBookable?: boolean;
};

type FilterType = keyof Filters;

type ChangeFilterValue = <FilterName extends FilterType>(
    filter: FilterName,
    value: Filters[FilterName]
) => void;

type FiltersContextState = Filters & {
    changeFilterValue: ChangeFilterValue;
};

const PRICE_MIN = 100;
const PRICE_MAX = 10000;

const FiltersContext = createContext<FiltersContextState>({
    price: {
        min: PRICE_MIN,
        max: PRICE_MAX,
    },
    types: [],
    changeFilterValue: () => {},
});

type ProviderProps = {
    children: ReactNode;
};

export const FiltersProvider = ({ children }: ProviderProps) => {
    const [filters, setFilters] = useState<Filters>({
        price: {
            min: PRICE_MIN,
            max: PRICE_MAX,
        },
        types: [],
    });

    const changeFilterValue: ChangeFilterValue = useCallback(
        (name, value) => {
            setFilters({ ...filters, [name]: value });
        },
        [filters]
    );

    const state = useMemo<FiltersContextState>(() => {
        return { ...filters, changeFilterValue };
    }, [filters, changeFilterValue]);

    return (
        <FiltersContext.Provider value={state}>
            {children}
        </FiltersContext.Provider>
    );
};

export const useFilters = () => {
    return useContext(FiltersContext);
};

export const Filters = () => {
    const { changeFilterValue, price, instantBookable, types } = useFilters();
    const { min, max } = price;

    const toggleType = (type: VanType) => {
        if (types.includes(type)) {
            changeFilterValue(
                'types',
                types.filter((item) => item !== type)
            );
        } else {
            changeFilterValue('types', [...types, type]);
        }
    };

    return (
        <FilterSection>
            <Container>
                <Wrapper>
                    <PriceGroup>
                        <Label>Cena za den</Label>
                        <Slider
                            sx={{
                                maxWidth: 485,
                                '& .MuiSlider-rail': {
                                    opacity: 1,
                                    background: 'var(--beige)',
                                },
                            }}
                            min={PRICE_MIN}
                            max={PRICE_MAX}
                            value={[min, max]}
                            onChange={(e, value) =>
                                Array.isArray(value)
                                    ? changeFilterValue('price', {
                                          min: value[0],
                                          max: value[1],
                                      })
                                    : changeFilterValue('price', {
                                          min: value,
                                          max: value,
                                      })
                            }
                        />
                        <InputGroup>
                            <TextField
                                type="number"
                                value={min}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            Kč
                                        </InputAdornment>
                                    ),
                                }}
                                onChange={(e) =>
                                    changeFilterValue('price', {
                                        min: parseInt(e.target.value),
                                        max,
                                    })
                                }
                                onBlur={(e) => {
                                    changeFilterValue('price', {
                                        min: clamp(min, PRICE_MIN, PRICE_MAX),
                                        max: clamp(max, PRICE_MIN, PRICE_MAX),
                                    });
                                }}
                            />
                            <TextField
                                type="number"
                                value={max}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            Kč
                                        </InputAdornment>
                                    ),
                                }}
                                onChange={(e) => {
                                    changeFilterValue('price', {
                                        min,
                                        max: parseInt(e.target.value),
                                    });
                                }}
                                onBlur={(e) => {
                                    changeFilterValue('price', {
                                        min: clamp(min, PRICE_MIN, PRICE_MAX),
                                        max: clamp(max, PRICE_MIN, PRICE_MAX),
                                    });
                                }}
                            />
                        </InputGroup>
                    </PriceGroup>
                    <FilterGroup>
                        <Label>Typ karavanu</Label>
                        <InputGrid>
                            <ToggleBox
                                onClick={() => toggleType(VanType.Campervan)}
                                isActive={types.includes(VanType.Campervan)}
                            >
                                <TypeName>Campervan</TypeName>
                                <TypeDescription>
                                    Obytka s rozměry osobáku, se kterou dojedete
                                    všude.
                                </TypeDescription>
                            </ToggleBox>
                            <ToggleBox
                                onClick={() => toggleType(VanType.Intergrated)}
                                isActive={types.includes(VanType.Intergrated)}
                            >
                                <TypeName>Integrál</TypeName>
                                <TypeDescription>
                                    Král mezi karavany. Luxus na kolech.
                                </TypeDescription>
                            </ToggleBox>
                            <ToggleBox
                                onClick={() => toggleType(VanType.BuiltIn)}
                                isActive={types.includes(VanType.BuiltIn)}
                            >
                                <TypeName>Vestavba</TypeName>
                                <TypeDescription>
                                    Celý byt geniálně poskládaný do dodávky.
                                </TypeDescription>
                            </ToggleBox>
                            <ToggleBox
                                onClick={() => toggleType(VanType.Alcove)}
                                isActive={types.includes(VanType.Alcove)}
                            >
                                <TypeName>Přívěs</TypeName>
                                <TypeDescription>
                                    Tažný karavan za vaše auto. Od kapkovitých
                                    až po rodinné.
                                </TypeDescription>
                            </ToggleBox>
                        </InputGrid>
                    </FilterGroup>
                    <FilterGroup>
                        <ReservationLabel>
                            <span>Okamžitá rezervace</span>&#8288;
                            <ActionIcon name="action" />
                        </ReservationLabel>
                        <Select
                            IconComponent={ExpandIcon}
                            sx={{ width: 176 }}
                            value={instantBookable ? 'true' : 'false'}
                            onChange={(e) =>
                                changeFilterValue(
                                    'instantBookable',
                                    e.target.value === 'true'
                                )
                            }
                        >
                            <MenuItem value="true">Ano</MenuItem>
                            <MenuItem value="false">Ne</MenuItem>
                        </Select>
                    </FilterGroup>
                </Wrapper>
            </Container>
        </FilterSection>
    );
};

const FilterSection = styled(Section)`
    border-bottom: 1px solid var(--beige);
    overflow: hidden;
`;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;

    ${queries.medium} {
        flex-direction: row;
    }
`;

const FilterGroup = styled.div`
    padding: 23px 0;
    flex: 1 1 auto;
    display: flex;
    flex-direction: column;
    gap: 16px;
    align-items: flex-start;

    ${queries.mobile} {
        width: 100%;
    }

    &:not(:last-child) {
        border-bottom: 1px solid var(--beige);
    }

    ${queries.medium} {
        &:not(:first-child) {
            padding-left: 16px;
        }

        &:not(:last-child) {
            border-bottom: none;
            border-right: 1px solid var(--beige);
            padding-right: 16px;
        }
    }

    input[type='number'] {
        &::-webkit-outer-spin-button,
        &::-webkit-inner-spin-button {
            appearance: none;
        }
    }
`;

const PriceGroup = styled(FilterGroup)`
    ${queries.medium} {
        flex: 0 1 400px;
    }
`;

const Label = styled.label`
    color: var(--dark-grey);
    text-align: left;
    font: normal normal normal 16px/16px Roboto;
    letter-spacing: 0px;
`;

const ReservationLabel = styled(Label)`
    white-space: nowrap;

    span:first-child {
        white-space: normal;
    }
`;

const ActionIcon = styled(Icon)`
    font-size: 16px;
    margin-bottom: -2px;
    margin-left: 8px;
`;

const InputGroup = styled.div`
    display: flex;
    gap: 16px;
`;

const InputGrid = styled.div`
    display: grid;
    gap: 16px;
    grid-template-columns: 1fr 1fr;

    ${queries.medium} {
        grid-template-columns: repeat(4, 1fr);
    }
`;

const ToggleBox = styled.div<{ isActive?: boolean }>`
    transition: border-color 0.2s ease;
    border-radius: 8px;
    max-width: 156px;
    border: 1px solid var(--beige);
    padding: 12px 11px 9px 13px;
    user-select: none;

    &:hover {
        cursor: pointer;
    }

    ${(p) =>
        p.isActive &&
        css`
            padding: 11px 10px 8px 12px;
            border: 2px solid var(--green);
        `}
`;

const TypeName = styled.p`
    color: var(--dark-blue);
    text-align: left;
    font: normal normal normal 16px/16px Roboto;
    letter-spacing: 0px;
    margin-bottom: 4px;
`;

const TypeDescription = styled.p`
    color: var(--dark-grey);
    text-align: left;
    font: normal normal normal 12px/14px Roboto;
    letter-spacing: 0px;
`;

const ExpandIcon = styled(Icon).attrs({ name: 'expand' })`
    font-size: 24px;
`;
