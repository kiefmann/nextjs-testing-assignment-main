import { Fragment, ReactNode } from 'react';
import styled from 'styled-components';
import { queries } from '../../../styles/viewports';

type Props<Item> = {
    renderItem: (item: Item) => ReactNode;
    items: Item[];
    onLoadMoreClick?: () => void;
    showLoadMoreButton: boolean;
};

export const ItemList = <Item extends {}>({
    onLoadMoreClick,
    items,
    renderItem,
    showLoadMoreButton,
}: Props<Item>) => {
    return (
        <>
            {items.length ? (
                <Grid>
                    {items.map((item, i) => (
                        <Fragment key={i}>{renderItem(item)}</Fragment>
                    ))}
                </Grid>
            ) : (
                <p>Žádné výsledky k zobrazení</p>
            )}
            {!!showLoadMoreButton && (
                <ButtonWrapper>
                    <LoadMoreButton type="button" onClick={onLoadMoreClick}>
                        Načíst další
                    </LoadMoreButton>
                </ButtonWrapper>
            )}
        </>
    );
};

const Grid = styled.div`
    display: grid;
    gap: 32px;
    grid-template-columns: repeat(auto-fill, minmax(290px, 1fr));

    ${queries.mobile} {
        grid-template-columns: 1fr;
    }
`;

const ButtonWrapper = styled.div`
    margin-top: 48px;
    margin-bottom: 16px;
    display: flex;
    justify-content: center;
`;

const LoadMoreButton = styled.button`
    background: var(--green);
    border-radius: 8px;
    min-width: 155px;
    min-height: 48px;
    display: flex;
    justify-content: center;
    align-items: center;
    color: var(--white);
    text-align: center;
    font: normal normal 900 16px/16px Roboto;
    border: none;
`;
