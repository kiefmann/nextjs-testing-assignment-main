import styled from 'styled-components';
import Image from 'next/image';
import { Container } from '../Container';
import { queries } from '../../../styles/viewports';

export const Header = () => {
    return (
        <HeaderComponent>
            <HeaderContainer>
                <Image
                    src={require('./assets/Prague Labs logo.svg')}
                    width={201}
                    height={35.19}
                    alt="Prague Labs logo"
                />
            </HeaderContainer>
        </HeaderComponent>
    );
};

const HeaderComponent = styled.header`
    width: 100%;
    border-bottom: 1px solid var(--beige);
`;

const HeaderContainer = styled(Container)`
    min-height: 80px;
    display: flex;
    align-items: center;

    ${queries.mobile} {
        justify-content: center;
    }
`;
