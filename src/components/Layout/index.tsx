import { ReactNode } from 'react';
import styled from 'styled-components';
import { Header } from '../Header';

type Props = {
    children: ReactNode;
};

export const Layout = ({ children }: Props) => {
    return (
        <Wrapper>
            <Header />
            <main>{children}</main>
        </Wrapper>
    );
};

const Wrapper = styled.div`
    min-height: 100vh;
    display: flex;
    flex-direction: column;

    main {
        flex: 1 0 auto;
    }
`;
