export enum VanType {
    Campervan = 'Campervan',
    Intergrated = 'Intergrated',
    BuiltIn = 'BuiltIn',
    Alcove = 'Alcove',
}

export type Van = {
    location: string;
    instantBookable: boolean;
    name: string;
    passengersCapacity: number;
    sleepCapacity: number;
    price: number;
    toilet: boolean;
    shower: boolean;
    vehicleType: VanType;
    pictures: string[];
};
