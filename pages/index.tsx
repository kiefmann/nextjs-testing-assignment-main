import { useCallback, useState } from 'react';
import styled from 'styled-components';
import { Container } from '../src/components/Container';
import {
    Filters,
    FiltersProvider,
    useFilters,
} from '../src/components/Filters';
import { ItemList } from '../src/components/ItemList';
import { Layout } from '../src/components/Layout';
import { Meta } from '../src/components/Meta';
import { Section } from '../src/components/Section';
import { VanCard } from '../src/components/VanCard';
import { Van } from '../src/types/van';

export const getServerSideProps = async () => {
    const response = await fetch(
        `${process.env.API_URL || 'http://localhost:3000/api'}/data`
    );

    let data = null;

    try {
        data = await response.json();
    } catch (e) {}

    return {
        props: {
            data,
        },
    };
};

type Props = {
    data: {
        count: number;
        items: Van[];
    } | null;
};

const INITIAL_SHOWN_ITEMS_COUNT = 6;
const SHOWN_ITEMS_COUNT_INCREMENT = 6;

const Home = ({ data }: Props) => {
    const [shownItemsCount, setShownItemsCount] = useState(
        INITIAL_SHOWN_ITEMS_COUNT
    );

    const { price, types, instantBookable } = useFilters();

    const filteredItems =
        data?.items
            .filter(
                (item) =>
                    item.instantBookable === instantBookable &&
                    (!types.length || types.includes(item.vehicleType)) &&
                    price.min <= item.price &&
                    price.max >= item.price
            )
            .slice(0, shownItemsCount) || [];
    const totalCount = filteredItems.length;

    const onLoadMoreClick = useCallback(() => {
        setShownItemsCount(
            Math.min(shownItemsCount + SHOWN_ITEMS_COUNT_INCREMENT, totalCount)
        );
    }, [shownItemsCount, totalCount]);

    return (
        <Layout>
            <Meta title="Karavany" description="Prague Labs testovací zadání" />
            <Filters />
            {data ? (
                <ListSection>
                    <Container>
                        <ItemList<Van>
                            renderItem={(item) => <VanCard {...item} />}
                            items={filteredItems}
                            onLoadMoreClick={onLoadMoreClick}
                            showLoadMoreButton={totalCount > shownItemsCount}
                        />
                    </Container>
                </ListSection>
            ) : (
                <Section>
                    <Container>
                        <p>Něco se pokazilo</p>
                    </Container>
                </Section>
            )}
        </Layout>
    );
};

const WrappedHome = (props: Props) => {
    return (
        <FiltersProvider>
            <Home {...props} />
        </FiltersProvider>
    );
};

const ListSection = styled(Section)`
    padding: 32px 0;
`;

export default WrappedHome;
