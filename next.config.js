/** @type {import('next').NextConfig} */
const withTM = require('next-transpile-modules')([
    '@mui/material',
    '@mui/system',
]);

module.exports = withTM({
    reactStrictMode: true,
    images: {
        domains: ['d35xwkx70uaomf.cloudfront.net'],
    },
    webpack: (config) => {
        config.resolve.alias = {
            ...config.resolve.alias,
            '@mui/styled-engine': '@mui/styled-engine-sc',
        };
        return config;
    },
});
